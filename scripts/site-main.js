// Requires jQuery to be loaded.
$(function() {
	var win = $(window),
		mainHeader = $('#main-header'),
		innerHeader = $('#inner-header'),
		hero = $('#hero');


	/**
	 * set the sticky nav if the window loads mid page
	 */
	mainHeader.height(innerHeader.outerHeight());
	if(win.scrollTop() > mainHeader.offset().top + mainHeader.height()){
		innerHeader.addClass('floating hidden');
	}else{
		innerHeader.removeClass('floating hidden');
	}

	if(win.scrollTop() > hero.offset().top + hero.height()){
		innerHeader.removeClass('hidden').addClass('in');
	}else if(win.scrollTop() > mainHeader.offset().top + mainHeader.height()){
		innerHeader.addClass('hidden');
	}else{
		innerHeader.removeClass('in');
	}

	/* ---- Smooth Scrolling ---- */
	$('a[href*="#"]:not([href="#"])').on("click touchend", function(event) {
		if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
			var target = $(this.hash);
			target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
			if (target.length) {
				$('html,body').animate({ scrollTop: target.offset().top - 100 }, 1000);
				return false;
			}
		}
	});
	/* ---- End Smooth Scrolling ---- */

	/* ---- Nav class for child focus ---- */
	$('nav[role=navigation] ul ul a').focus(function(){
		var that = $(this);

		that.closest('ul').prev('a').addClass('child-has-focus');
	}).blur(function(){
		var that = $(this);

		that.closest('ul').prev('a').removeClass('child-has-focus');
	});

	/**
	 * set the sitcky nav when the window scrolls
	 * @param  {object} e event
	 * @return   none
	 */
	win.on('scroll', function(e){
		if(win.scrollTop() > mainHeader.offset().top + mainHeader.height()){
			innerHeader.addClass('floating hidden');
		}else{
			innerHeader.removeClass('floating hidden');
		}

		if(win.scrollTop() > hero.offset().top + hero.height()){
			innerHeader.removeClass('hidden').addClass('in');
		}else if(win.scrollTop() > mainHeader.offset().top + mainHeader.height()){
			innerHeader.addClass('hidden');
		}else{
			innerHeader.removeClass('in');
		}
	});

	$('#burger, .close-sidebar').click(function(e){
		e.preventDefault();
		$('#burger, header[role="banner"] nav').toggleClass('show');

		if(win.outerWidth() <= 600){
			$('body').toggleClass('no-scroll');
		}
	});

	$.ajax({
		url: "http://ajaxhttpheaders.appspot.com",
		dataType: 'jsonp',
		success: function(headers){
			var switcher = $('#language-switcher'),
				language;

			language = headers['Accept-Language'];
			language = language.split(',')[0];
			switcher.find("[value='" + language + "']").attr('selected');
			switcher.addClass(language);
		}
	});
});

// Sitewide functions
